import csv
import os.path

country_dic = dict()

iso_group_filename = 'iso_group.txt'


def create_empty_country_dict(file_path):
    with open(file_path, 'r') as f:
        country_dict = {i: set() for i in map(str.strip, f.readlines())}
    return country_dict


def read_from_csv(csv_path):
    dict_name = dict()
    with open(csv_path, 'r') as csv_file:
        file_info = csv.reader(csv_file)
        for row in file_info:
            row_info = row[0].split('.')
            dict_name[row_info[0]] = set(row_info[1:])
        return dict_name


def write_to_csv(file_path, dict_name):
    with open(file_path, 'w') as csv_file:
        file_writer = csv.writer(csv_file, delimiter='.', quoting=csv.QUOTE_NONE, escapechar='.', quotechar='.')
        for key, value in sorted(dict_name.items()):
            try:
                file_writer.writerow([key] + list(value))
            except:
                print 'Could not write rows to csv file'
                return None


def is_csv_available(filename):
    return True if os.path.isfile(filename) else False


if is_csv_available(iso_group_filename):
    country_dic = read_from_csv(iso_group_filename)
else:
    country_dic = create_empty_country_dict('iso')

a = ('2', ['BD', 'BG', 'BN'])
b = ('5', ['BD', 'BA', 'CH', 'CK', 'CM'])
c = ('7', set(country_dic.keys()) - set(a[1]) - set(b[1]))



for i in [a, b, c]:
    for j in i[1]:
        country_dic[j].add(i[0])

write_to_csv(iso_group_filename, country_dic)

