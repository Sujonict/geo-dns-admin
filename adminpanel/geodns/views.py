from django.shortcuts import render
from .process_admin import get_country_dict, update_country_group

from django.http import HttpRequest, QueryDict
from .models import table_group_ip, table_host_group
from django.utils.encoding import smart_unicode
from datetime import datetime
import os.path
from .database import *

# Create your views here.
iso_group = 'iso_group.txt'

module_dir = os.path.dirname(__file__)  # get current directory
iso_group_filename = os.path.join(module_dir, iso_group)



def all_views(request):

    context = {
        "countries_list": get_country_dict().keys(),
    }

    return render(request, "index.html", dict(context=context))


# select * from geodns_table_host_group where host = "sujon.com";
def ip_form(request):
    # host_group = table_host_group.objects.raw('select * from geodns_table_host_group group by host')
    host_group =  get_host_group()
    # hos_test = table_host_group.objects.raw('select * from geodns_table_host_group')
    hos_test = get_all_host()
    test = {}
    for i in host_group:
        test[i.host] = []
        print("host {0}".format(i.host))
    for i in hos_test:
        for j in host_group:
            if j.host in i.host:
                test[j.host].append(i.group_name)

    print(host_group)
    print(test)
    context = {
        "hosts": host_group,
        "host_groups": test
    }
    return render(request, "ip_form.html", dict(context=context))


def country_form(request):
    # host_group = table_host_group.objects.raw('select * from geodns_table_host_group group by host')
    host_group = get_host_group()
    # hos_test = table_host_group.objects.raw('select * from geodns_table_host_group')
    hos_test = get_all_host()
    test = {}
    for i in host_group:
        test[i.host] = []
        print("host {0}".format(i.host))
    for i in hos_test:
        for j in host_group:
            if j.host in i.host:
                test[j.host].append(i.group_name)

    print(host_group)
    print(test)
    context = {
        "hosts": host_group,
        "host_groups": test,
        "countries_list": get_country_dict().keys(),
    }
    return render(request, "country_form.html", dict(context=context))


def group_form(request):
    if request.method == 'POST':
        hostname = request.POST["hostname"]
        no_region = request.POST["no_region"]
        print(hostname, type(no_region))
        group =  get_specific_host(hostname)
        # group = table_host_group.objects.raw('SELECT * from geodns_table_host_group WHERE Host = %s', [hostname])
        for g in group:
            print("ID {0}".format(g.id))
        context = {
            "hostname": hostname,
            "no_region": range(1, int(no_region) + 1),
            "regions": int(no_region),
            "exist_groups": group,
            "countries_list": get_country_dict().keys(),
        }
    return render(request, "group_form.html", dict(context=context))


def group_post(request):
    if request.method == 'POST':
        hostname = request.POST["hostname"]
        no_region = request.POST["regions"]
        print(no_region)
        group_list = []
        for region in range(int(no_region)):
            group_list.append(request.POST[str(region + 1)])
        #context = {}
        print(type(group_list))
        for i in group_list:
            # group_update = table_host_group(group_name=i, host=hostname)
            # group_update.save()
             group_add(i, hostname)

        context = {
            "hostname": hostname,
            "groups" :  get_specific_host(hostname),
        }
    return render(request, "group_ip.html", dict(context=context))


def ip_update(request):
    if request.method == "POST":
        hostname = request.POST["hostname"]
        group_name = request.POST["group_name"]
        if (hostname == None or group_name == "--Select--" or group_name == None ):
            return ip_form(request)
        print(hostname, group_name)
        # select * from geodns_table_host_group where host="sujon.com" and group_name="ring";
        group =  get_group_info(hostname, group_name)
        # group = table_host_group.objects.raw(
        #     'SELECT * from geodns_table_host_group WHERE Host = %s and group_name = %s', [hostname, group_name])
        print("groups", group)
        for ips in group:
            print("ips", ips.id)
        # ip_list = table_group_ip.objects.raw(
        #     'select * from geodns_table_group_ip where ip_group_id = {0}'.format(ip.id))
        #ip_list =  get_ip_info(ips.id)
        # print('select * from geodns_table_group_ip where ip_group_id = {0}'.format(i.id)
        # for i in ip_list:
        #     print("iplist-->", i.ip

    context = {
        "ip_list":  get_ip_info(ips.id),
        "group_id": ips.id,
        "host" : hostname,
        "group" : group_name
    }

    return render(request, "ip_update.html", dict(context=context))


def ip_post(request):
    if request.method == "POST":
        group_id = request.POST["group_id"]
        ip_val = request.POST["ip"]
        type_list = request.POST["type"]
        priority_list = request.POST["priority"]
        if request.POST["ip_ttl"] == "custom":
            ip_ttl = request.POST["custom"]
        else:
            ip_ttl = request.POST["ip_ttl"]
        ip_add(group_id, ip_val, type_list, priority_list, ip_ttl)
        # for i, j, k, l in zip(ip_list, type_list, priority_list, ttl_list):
        #     # ip_list_update = table_group_ip(ip_group_id=group_id, ip=i, type=j, priority=k, ttl=l)
        #     # ip_list_update.save()
        #      ip_add(group_id, i, j, k, l)
        print(ip_val, group_id)
    # context = {}
    return ip_update(request)


def group_find(request):
    if request.method == 'POST':
        hostname = request.POST["hostname"]
        # test = table_host_group.objects.raw('SELECT * from geodns_table_host_group WHERE Host = %s', [hostname])
        test =  get_specific_host(hostname)
        if test != None:
            flag = 1
        context = {
            "hostname": hostname,
            # "groups": table_host_group.objects.raw('SELECT * from geodns_table_host_group WHERE Host = %s', [hostname]),
            "groups" :  get_specific_host(hostname),
            # "hosts": table_host_group.objects.raw(' select * from geodns_table_host_group group by host'),
            "hosts" :  get_host_group(),
            "countries": get_country_dict().keys(),
            "flag": 1,
            "row": range(13),
            "col": range(20)
        }
    return render(request, "country_form.html", dict(context=context))


def country_update(request):
    if request.method == 'POST':
        hostname = request.POST["hostname"]
        # groups = table_host_group.objects.raw('SELECT * from geodns_table_host_group WHERE Host = %s', [hostname])
        groups =  get_specific_host(hostname)
        group_list = []
        print("test", groups)
        for group in groups:
            group_list.append((group.id, map(str, request.POST.getlist("{0}[]".format(group)))))
        list_without_default_group = []
        for i in range(len(group_list) - 1):
            list_without_default_group.extend(group_list[i][1])
        default_group_list = set(get_country_dict().keys()) - set(list_without_default_group)
        group_list[-1] = (group_list[-1][0], default_group_list)
        update_country_group(group_list)
        context = {}
    return render(request, "success.html", dict(context=context))


def group_country(iso_group_filename):
    with open(iso_group_filename, 'r') as f:
        line_list = [line.strip() for line in f]
    all_dict = dict()

    for line in line_list:
        single_list = line.split('.')
        for group_id in single_list[1:]:
            # group_name_object = table_host_group.objects.raw('select id, group_name from geodns_table_host_group where id=%s', [group_id])
            group_name_object =  get_id_group_name(group_id)
            for group_name in group_name_object:
                all_dict.setdefault(str(group_name.group_name), []).append(single_list[0])
    return all_dict


def home(request):
    # host_group_list = table_host_group.objects.raw('SELECT * from geodns_table_host_group')
    host_group_list =  get_all_host()
    ip_group_dict = dict()
    info_dict = {}
    temp_list = []
    for i in host_group_list:
        info_dict[str(i.host)] = {}
        # gip = table_host_group.objects.raw('SELECT * from geodns_table_group_ip where ip_group_id = %s', [i.id])
        gip =  get_ip_info(i.id)
        for j in gip:
            temp_list.append((str(i.group_name), str(j.ip)))
    for data in temp_list:
        if data[0] in ip_group_dict:
            ip_group_dict[data[0]].append(data[1])
        else:
            ip_group_dict[data[0]] = [data[1]]
    for data in host_group_list:
        if data.group_name in ip_group_dict.keys():
            info_dict[str(data.host)][str(data.group_name)] = ip_group_dict[str(data.group_name)]

    group_country_dict = group_country(iso_group_filename)

    print("all %s " % group_country_dict)
    context = {
        "group_ip": info_dict,
        "group_country": group_country_dict
    }
    return render(request, 'view_all.html', dict(context=context))

def edit(request, ip, priority, ip_type, ttl, hostname, group):

    context = {
        "ip" : ip,
        "priority" : priority,
        "type" : ip_type,
        "ttl" : ttl,
        "hostname" : hostname,
        "group_name" : group
    }
    return render(request, 'ip_modify.html', dict(context=context))


def ip_modify(request):
    if request.method == 'POST':
        ip = request.POST["ip"]
        ip_type = request.POST["type"]
        priority = request.POST["priority"]
        ttl = request.POST["ttl"]
        if request.POST["ttl"] == "custom":
            ip_ttl = request.POST["custom"]
        else:
            ip_ttl = request.POST["ttl"]
       # time_in_sec = ttl.split(" - ")
        #print("time list---------------------->", time_in_sec[0]
       # result_ttl= (datetime.strptime(str(time_in_sec[1]), '%m/%d/%Y %H:%M %p') - datetime.strptime(str(time_in_sec[0]), '%m/%d/%Y %H:%M %p')).seconds
       # print("seconds----------------------->", result_ttl
        #print("time --->", result_ttl
        old_ip =  request.POST["old_ip"]

        ip_update(old_ip, ip, ip_type, priority, ip_ttl)
        # table_group_ip.objects.filter(ip = old_ip).update(ip = ip, type= ip_type, priority=priority, ttl=ttl)
        #old_ip = 49
        #ip_modified = table_group_ip.objects.raw('update geodns_table_group_ip set ip=%s, type=%s, priority=%s, ttl=%s where id=%s', [ip, ip_type, priority, ttl, old_ip])
        #print(ip_modified
        # for i in ip_modified:
        #     print(i
        print(ip, ip_type, priority, ttl)
        print("actual ip", request.POST["old_ip"])

    return ip_update(request)


def delete_ip(request):
    if request.method == 'POST':
        ip = request.POST["ip"]
        delete_ip(ip)
    return ip_update(request)



