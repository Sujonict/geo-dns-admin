# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('geodns', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='table_group_ip',
            name='priority',
            field=models.CharField(max_length=5, null=True),
        ),
        migrations.AddField(
            model_name='table_group_ip',
            name='ttl',
            field=models.CharField(max_length=3, null=True),
        ),
    ]
