# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='table_group_ip',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ip', models.CharField(max_length=15)),
                ('type', models.CharField(max_length=4)),
            ],
        ),
        migrations.CreateModel(
            name='table_host_group',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('group_name', models.CharField(max_length=150)),
                ('host', models.CharField(max_length=250)),
            ],
        ),
        migrations.AddField(
            model_name='table_group_ip',
            name='ip_group',
            field=models.ForeignKey(to='geodns.table_host_group'),
        ),
    ]
