from django.contrib import admin

# Register your models here.
from .models import table_host_group, table_group_ip


class table_host_groupAdmin(admin.ModelAdmin):
    class Meta:
        model = table_host_group


class table_group_ipAdmin(admin.ModelAdmin):
    class Meta:
        model = table_group_ip

admin.site.register(table_host_group, table_host_groupAdmin)
admin.site.register(table_group_ip, table_group_ipAdmin)
