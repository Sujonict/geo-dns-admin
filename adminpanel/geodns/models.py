from django.db import models

# Create your models here.

class table_host_group(models.Model):
    group_name = models.CharField(max_length=150)
    host = models.CharField(max_length=250)

    def __unicode__(self):
        return "{0} {1}".format(self.group_name, self.host)


class table_group_ip(models.Model):
    ip_group = models.ForeignKey(table_host_group)
    ip = models.CharField(max_length=15)
    type = models.CharField(max_length=4)
    priority = models.CharField(max_length=5,null=True)
    ttl = models.CharField(max_length=3,null=True)

    def __unicode__(self):
        return "group: {0} >> IP : {1} : {2} {3} {4}".format(self.ip_group, self.ip, self.type, self.priority, self.ttl)





