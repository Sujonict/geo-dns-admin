__author__ = 'panthotanvir'


from .models import table_group_ip, table_host_group


def get_all_host():

    hosts_info = table_host_group.objects.raw(
        'select * from geodns_table_host_group')

    return hosts_info

def get_host_group():

    host_group = table_host_group.objects.raw(
        'select * from geodns_table_host_group group by host')

    return host_group

def get_specific_host(hostname):

    host_info = table_host_group.objects.raw(
        'SELECT * from geodns_table_host_group WHERE Host = %s', [hostname])

    return host_info

def get_group_info(hostname, group_name):

    group_info = table_host_group.objects.raw(
            'SELECT * from geodns_table_host_group WHERE Host = %s and group_name = %s', [hostname, group_name])

    return group_info

def get_ip_info(ip):

    ip_info = table_group_ip.objects.raw(
            'select * from geodns_table_group_ip where ip_group_id = {0}'.format(ip))

    return ip_info

def get_id_group_name(group_id):

    id_group_name = table_host_group.objects.raw('select id, group_name from geodns_table_host_group where id=%s', [group_id])

    return id_group_name

def group_add(group_name, hostname):

    group_update = table_host_group(group_name=group_name, host=hostname)
    group_update.save()

def ip_add(group_id, ip, type, priority, ttl):

    ip_list_update = table_group_ip(ip_group_id=group_id, ip=ip, type=type, priority=priority, ttl=ttl)
    ip_list_update.save()

def ip_update(old_ip, ip, ip_type, priority, ttl ):

    table_group_ip.objects.filter(ip = old_ip).update(ip = ip, type= ip_type, priority=priority, ttl=ttl)

def delete_ip(ip):

    table_group_ip.objects.filter(ip=ip).delete()