"""adminpanel URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [

    url(r'^$','geodns.views.home'),
    url(r'^new_group/$', 'geodns.views.all_views', name="new_group"),
    url(r'^ip_form/$','geodns.views.ip_form', name="ip_form"),
    url(r'^country_form/$','geodns.views.country_form', name="country_form"),
    url(r'^group_form/$','geodns.views.group_form', name="group_form"),
    url(r'^group_post/$','geodns.views.group_post', name="group_post"),
    url(r'^ip_update/$','geodns.views.ip_update', name="ip_update"),
    url(r'^ip_modify/$','geodns.views.ip_modify', name="ip_modify"),
    url(r'^ip_post/$','geodns.views.ip_post', name="ip_post"),
    url(r'^group_find/$','geodns.views.group_find', name="group_find"),
    url(r'^country_update/$','geodns.views.country_update', name="country_update"),
    url(r'^edit/(?P<ip>.*)/(?P<priority>.*)/(?P<ip_type>.*)/(?P<ttl>.*)/(?P<hostname>.*)/(?P<group>.*)/$', 'geodns.views.edit', name="edit"),
    url(r'^delete_ip/$', 'geodns.views.delete_ip', name="delete_ip"),
    url(r'^admin/', include(admin.site.urls)),


]
